-module(server).
-author("rwalski, mpiotrowski").

%-export([]).
-compile([export_all]).
-import(semaphore, [start/0, stop/0, signal/0, wait/0]).
-import(store, [startDB/0]).

%% TODO: GetUNameById
%% TODO: GetBookNameById
%%starts server and semaphore
startServer(Port) ->
  semaphore:start(),

  Pid = spawn_link(fun() ->
    doStartServer(Port)
                   end),
  {ok, Pid}.

%%cretes listening socket on given port and spawns first accepting function
doStartServer(Port) ->
  {ok, Listen} = gen_tcp:listen(Port, [binary, {active, false}]),
  store:startDB(),
  spawn(fun() -> acceptor(Listen) end),
  timer:sleep(infinity).

%%accepting connection, checks if any librarian is free, and handles it if yes
acceptor(Listen) ->
  {ok, Socket} = gen_tcp:accept(Listen),
  semaphore:wait(),
  spawn(?MODULE, acceptor, [Listen]),
  handle(Socket).


handle(Socket) ->
  Id = getIdFromUser(Socket),
  printOptions(Socket),
  handle(Socket, Id).


getIdFromUser(Socket) ->
  gen_tcp:send(Socket, "Insert your ID please. Write \"Id: <id>\"\n"),
  inet:setopts(Socket, [{active, once}]),
  receive
    {tcp, Socket, <<"quit", _/binary>>} ->
      gen_tcp:close(Socket),
      semaphore:signal(),
      quit;
    {tcp, Socket, <<"register", _/binary>>} ->
      registerUser(Socket);
    {tcp, Socket, WrappedId} ->
      Id = checkAndExtractId(WrappedId, Socket),
      IsRegistered = store:isRegistered(Id),
      case IsRegistered of
        false -> gen_tcp:send(Socket, "You've not been recognised."),
          gen_tcp:close(Socket),
          semaphore:signal(),
          quit;
        _ ->
          gen_tcp:send(Socket, "You've been recognised as: "),
          gen_tcp:send(Socket, Id),
          gen_tcp:send(Socket, "\n")

      end,
      Id;
    _ ->
      semaphore:signal(),
      quit
  end.



printOptions(Socket) ->
  gen_tcp:send(Socket, "To quit type 'quit'\n"),
  gen_tcp:send(Socket, "To borrow a book type 'book: \"<Id>\" \n"),
  gen_tcp:send(Socket, "To return a book type: return: \"<Id>\" \n\n").


handle(Socket, Id) -> %% Id means user's id
  io:format("ID ~s~n", [Id]),
  inet:setopts(Socket, [{active, once}]),
  receive
    {tcp, Socket, <<"quit", _/binary>>} ->
      gen_tcp:close(Socket),
      semaphore:signal();
    {tcp, Socket, Msg} ->
      TextMsg = binary_to_list(Msg),
      StrippedId = string:strip(Id, both, $\n),
      StrippedId2 = string:strip(StrippedId, both, $\r),
      BorrowOp = (string:str(TextMsg, "book:") /= 0),
%%      ReturnOp = (string:str(TextMsg, "return:") /= 0),

      case BorrowOp of
        true ->
          End = string:rchr(TextMsg, $\r)- 1,
          BookId = string:sub_string(TextMsg, 7, End),
          io:format("BookID: ~s", [BookId]),
          case store:borrowBook(StrippedId2, BookId) of
            "Successful" -> gen_tcp:send(Socket, "Enjoy your book.");
            "Book is borrowed" -> gen_tcp:send(Socket, "The book has already been borrowed.");
            "Book not found" -> gen_tcp:send(Socket, "The book specified has not been found.");
            _ -> gen_tcp:send(Socket, "Unknown Error")
          end
      end,
%%
%%      case ReturnOp of
%%        true ->
%%      end,

      gen_tcp:send(Socket, Msg),
      handle(Socket, Id);

%%      gen_tcp:close(Socket),
%%      semaphore:signal(),
%%      quit;
      _ ->
      semaphore:signal()
  end.

checkAndExtractId(Id, Socket) ->
  I = string:str(binary_to_list(Id), "Id: "), %% must be 1
  case I of
    1 -> RealId = string:substr(binary_to_list(Id), 5);
    _ -> RealId = "-1"
  end,
  RealId.

%% Checks if user input conforms established form

registerUser(Socket) ->
  gen_tcp:send(Socket, "Please insert an ID and a full name (write: ID firstname surname)"),
  inet:setopts(Socket, [{active, once}]),
  receive
    {tcp, Socket, <<"quit", _/binary>>} ->
      gen_tcp:close(Socket),
      semaphore:signal();
    {tcp, Socket, Msg} ->
      MsgList = binary_to_list(Msg),
      case conformsIdNamePolicy(MsgList) of
        true ->
          Stripped = string:strip(MsgList, both, $ ),
          [ID, FName, SName] = string:tokens(Stripped, " "),
          trulyRegisterUser(Socket, ID, FName, SName);
        false ->
          io:format("User input doesn't conform to the established format\n"),
          gen_tcp:close(Socket),
          semaphore:signal()
      end;
    _ ->
      semaphore:signal()
  end.

conformsIdNamePolicy(Str) ->
  Stripped = string:strip(Str, both, $ ),
  Tokens = string:tokens(Stripped, " "),
  NumOfTokens = length(Tokens),
  io:format("Received: ~s", [Tokens]),
  case NumOfTokens == 3 of
    true -> io:format("numoftokens: ~w\n", [NumOfTokens]), true;
    false -> io:format("numoftokens: ~w\n", [NumOfTokens]), false
  end.

%% should update DB
trulyRegisterUser(Socket, ID, Name, Surname) ->
  FullName = Name ++ " " ++ Surname,
  store:registerUser(ID, FullName), %% loops a newDB
  gen_tcp:send(Socket, "ID code "),
  gen_tcp:send(Socket, ID),
  gen_tcp:send(Socket, " has been registered with name "),
  gen_tcp:send(Socket, FullName),
  gen_tcp:send(Socket, "\n").



